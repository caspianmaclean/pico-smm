from time import sleep

### resources used ###

# resources used by the keyboard section:
# sys.stdin

# resources used by the audio section:
audio_left_pin = 28 
audio_right_pin = 27
# PWM peripheral(s) associated with the two pins

# resources used by the VGA section:
video_statemachine = 0 # the index of the state machine used for video
video_pin_out_base = 10 # first (lowest number) video output pin
video_pin_out_count = const(8) # including colour pins and sync pins
# allocating pins 10 to 17 allows using 8 pins
# with GPIO 16 and 17 being HSYNC and VSYNC as in the VGA demo board.
# DMA channels for video
main_DMA_CH = 1
ctrl_DMA_CH = 2
spare_DMA_CH = 3

### end list of resources used

### keyboard section - using stdin instead of keyboard ###

import select
import sys

key_poll=select.poll()
key_poll.register(sys.stdin,select.POLLIN)
def inkeys():
    tmp=""
    kp=key_poll.poll(0)
    if len(kp) > 0:
        # possibly should check contents of kp to see if there's no character to read
        tmp=sys.stdin.read(1)
        kp=key_poll.poll(0)
    return tmp

### end keyboard section ###

### audio section - very simple ###

from time import sleep
from machine import Pin,PWM

audio_left = Pin(audio_left_pin, Pin.OUT)
audio_right = Pin(audio_right_pin, Pin.OUT)

pwm_audio_left = PWM(audio_left)
pwm_audio_right = PWM(audio_right)

def beep2(freq,time):
    pwm_audio_left.freq(freq)
    pwm_audio_right.freq(freq)
    pwm_audio_left.duty_u16(32768)
    pwm_audio_right.duty_u16(32768)
    sleep(time)
    pwm_audio_left.deinit()
    pwm_audio_right.deinit()

def quick_beep(freq):
    beep2(freq,0.1)

### end audio section ###
    
### VGA section - low resolution ###
import rp2
import machine
import uctypes

# PIO related addresses and numbers for statemachine
PIO0_BASE = 0x50_200_000
video_TXF_addr = PIO0_BASE + 0x010
DREQ_PIO0_TX0 = 0
video_PIO_DREQ = DREQ_PIO0_TX0

# tinyvga.com for 640x480 60 Hz VGA video timing

# horizontal timing
video_display_width=640
video_signal_front_porch_width=16
video_signal_hsync_width=96
video_signal_back_porch_width=48
video_signal_total_width=800

# vertical timing
video_display_height=480
video_signal_front_porch_height=10
video_signal_vsync_height=2
video_signal_back_porch_height=33
video_signal_total_height=525

video_hsync_bit_high = 1 << 6
video_vsync_bit_high = 1 << 7
video_hsync_bit_low=0
video_vsync_bit_low=0

# tinyvga.com for 640x480 60 Hz VGA video sync polarity
video_hsync_active=video_hsync_bit_low
video_vsync_active=video_vsync_bit_low

video_hsync_inactive=video_hsync_active^video_hsync_bit_high
video_vsync_inactive=video_vsync_active^video_vsync_bit_high

video_colour_mask=(1<<6)-1
video_colour_r_mask=1+2
video_colour_g_mask=4+8
video_colour_b_mask=16+32
video_colour_blank=0

video_hsync_start_x=video_display_width+video_signal_front_porch_width
video_vsync_start_y=video_display_height+video_signal_front_porch_height

pixel_dup_factor = const(8)

# pull an 8 bit byte at a time if video_pin_out_count is const(8)
@rp2.asm_pio(out_init=(rp2.PIO.OUT_LOW, rp2.PIO.OUT_LOW, rp2.PIO.OUT_LOW, rp2.PIO.OUT_LOW, rp2.PIO.OUT_LOW, rp2.PIO.OUT_LOW, rp2.PIO.OUT_LOW, rp2.PIO.OUT_LOW), autopull=True, pull_thresh=video_pin_out_count)
def pio_video_out():
    pull(block)
    wrap_target()
    # want time between outputs of 5 * pixel_dup_factor
    # 5 works with 125MHz PIO clock and 25MHz pixel clock
    out(pins, video_pin_out_count) [pixel_dup_factor-1]
    nop()                          [pixel_dup_factor-1]
    nop()                          [pixel_dup_factor-1]
    nop()                          [pixel_dup_factor-1]
    nop()                          [pixel_dup_factor-1]
    wrap()

green=video_colour_g_mask
white=video_colour_mask
black=0

video_signal_map_size = video_signal_total_width * video_signal_total_height // pixel_dup_factor

video_signal_map = bytearray(video_signal_map_size)

blank_signal = video_colour_blank + video_hsync_inactive + video_vsync_inactive
hsync_signal = video_colour_blank + video_hsync_active + video_vsync_inactive
vsync_signal = video_colour_blank + video_hsync_inactive + video_vsync_active
vhsync_signal = video_colour_blank + video_hsync_active + video_vsync_active

video_hsync_end_x = video_hsync_start_x+video_signal_hsync_width
video_vsync_end_y = video_vsync_start_y+video_signal_vsync_height

# full screen (ignoring sync)
i=0 # index of next unfilled entry in video_signal_map
for y in range(0,video_signal_total_height):
    for x in range(0, video_signal_total_width, pixel_dup_factor):
        video_signal_map[i] = blank_signal
        i+=1

# hsync
for y in range(0,video_signal_total_height):
    i=((y * video_signal_total_width+video_hsync_start_x) // pixel_dup_factor)
    for x in range(video_hsync_start_x, video_hsync_end_x, pixel_dup_factor):
        video_signal_map[i] = hsync_signal
        i+=1
        
# vsync
for y in range(video_vsync_start_y, video_vsync_end_y):
    i=y * video_signal_total_width // pixel_dup_factor
    for x in range(0, video_signal_total_width, pixel_dup_factor):
        video_signal_map[i] = vsync_signal
        i+=1

# vhsync
for y in range(video_vsync_start_y, video_vsync_end_y):
    i=((y * video_signal_total_width+video_hsync_start_x) // pixel_dup_factor)
    for x in range(video_hsync_start_x, video_hsync_start_x+video_signal_hsync_width, pixel_dup_factor):
        video_signal_map[i] = vhsync_signal
        i+=1

def plot_block(colour, x, y):
    # duplicate pixel_dup_factor rows
    # one memory location already represents pixel_dup_factor columns
    signal = (colour & video_colour_mask)
    signal += video_hsync_inactive 
    signal += video_vsync_inactive
    if x >= video_display_width  // pixel_dup_factor: return
    if y >= video_display_height // pixel_dup_factor: return
    y_fine_base = y*pixel_dup_factor
    row_length = video_signal_total_width // pixel_dup_factor
    for y_fine in range(y*pixel_dup_factor, (y+1)*pixel_dup_factor):
        i=row_length*y_fine+x
        video_signal_map[i]=signal

# create pio state machine
sm = rp2.StateMachine(video_statemachine)
sm.init(pio_video_out, out_base = video_pin_out_base)
# uses default clock frequency, which is default system clock frequency, which is 125 MHz

sm.active(1)
# start state machine, which will quickly reach a PULL
# instruction and wait for FIFO input

DMA_BASE=0x50_000_000

DMA_CH0_offset = 0
DMA_CH1_offset = 0x40
DMA_CH2_offset = 0x80
DMA_CH3_offset = 0xC0

DMA_CH_n_offset = {}
DMA_CH_n_offset[0] = DMA_CH0_offset
DMA_CH_n_offset[1] = DMA_CH1_offset
DMA_CH_n_offset[2] = DMA_CH2_offset
DMA_CH_n_offset[3] = DMA_CH3_offset

DMA_RD_offset = 0
DMA_WR_offset = 4
DMA_COUNT_offset = 8
DMA_CTRL_TRIG_offset = 0xC
DMA_CTRL_offset = 0x10
DMA_RD_TRIG_offset = 0x3C

SIZE_BYTE = 0
SIZE_HALFWORD = 1
SIZE_WORD = 2

TREQ_UNPACED = 0x3F

def dma_ctrl_reg_value(*, en=0, data_size=0, inc_read=0, inc_write=0, chain_to, treq):
    if en > 1:
        print("bad en value")
        en = 0
    if inc_read > 1:
        print("bad inc_read value")
        inc_read = 0
    
    return (en << 0) | (data_size << 2) | (inc_read << 4) | (inc_write << 5) | (chain_to << 11) | (treq << 15)

# main DMA channel fills the PIO buffer with video signal

main_DMA_CH_offset = DMA_CH_n_offset[main_DMA_CH]
main_DMA_CH_base = main_DMA_CH_offset + DMA_BASE

machine.mem32[main_DMA_CH_base+DMA_RD_offset] = 0
machine.mem32[main_DMA_CH_base+DMA_WR_offset] = video_TXF_addr
machine.mem32[main_DMA_CH_base+DMA_COUNT_offset] = video_signal_map_size
machine.mem32[main_DMA_CH_base+DMA_CTRL_offset] = dma_ctrl_reg_value(en=1, data_size=SIZE_BYTE, inc_read=1, chain_to=ctrl_DMA_CH, treq=video_PIO_DREQ)


main_DMA_RD_reg_addr = main_DMA_CH_base+DMA_RD_offset
# that register will need to be reset each frame, so it
# starts reading from the start of the display buffer again

# this DMA channel I'll keep some spare register data in.
# Probably should find a better way to do it.
spare_DMA_CH_offset = DMA_CH_n_offset[spare_DMA_CH]
spare_DMA_CH_base = spare_DMA_CH_offset + DMA_BASE

spare_DMA_RD_reg_addr = spare_DMA_CH_base+DMA_RD_offset

machine.mem32[spare_DMA_RD_reg_addr] = uctypes.addressof(video_signal_map)


# ctrl DMA channel resets main DMA channel read address.

ctrl_DMA_CH_offset = DMA_CH_n_offset[ctrl_DMA_CH]
ctrl_DMA_CH_base = ctrl_DMA_CH_offset + DMA_BASE

# ctrl DMA channel config, enabled, triggering with the final register write

machine.mem32[ctrl_DMA_CH_base+DMA_RD_offset] = spare_DMA_RD_reg_addr
machine.mem32[ctrl_DMA_CH_base+DMA_WR_offset] = main_DMA_RD_reg_addr
machine.mem32[ctrl_DMA_CH_base+DMA_COUNT_offset] = 1
machine.mem32[ctrl_DMA_CH_base+DMA_CTRL_TRIG_offset] = dma_ctrl_reg_value(en=1, data_size=SIZE_WORD, chain_to=main_DMA_CH, treq=TREQ_UNPACED)

# in case VGA output needs to stop, can interrupt the DMA chain
def stop_vga_dma():
    machine.mem32[ctrl_DMA_CH_base+DMA_CTRL_offset] = dma_ctrl_reg_value(en=1, data_size=SIZE_WORD, chain_to=ctrl_DMA_CH, treq=TREQ_UNPACED)

def restart_vga_dma():
    machine.mem32[ctrl_DMA_CH_base+DMA_CTRL_TRIG_offset] = dma_ctrl_reg_value(en=1, data_size=SIZE_WORD, chain_to=main_DMA_CH, treq=TREQ_UNPACED)

### end VGA section ###


def beep_top():
    quick_beep(100)
def beep_wall():
    quick_beep(200)
def beep_key_left():
    quick_beep(2000)
def beep_key_right():
    quick_beep(2500)

sleeptime = 0.05 # seconds


red = video_colour_r_mask
midred = (2*red//3) & video_colour_r_mask
darkred = (red//3) & video_colour_r_mask

midgreen = (2*green//3) & video_colour_g_mask
darkgreen = (green//3) & video_colour_g_mask

blue = video_colour_b_mask
midblue = (2*blue//3) & video_colour_b_mask
darkblue = (blue//3) & video_colour_b_mask

bg=black
ball_colour=green
trail_1=midgreen
trail_2=darkgreen

bg=darkgreen

def bounce():
    global bg
    global ball_colour
    global trail_1
    global trail_2
    x=3
    y=3
    dx=1
    dy=1
    ox=x
    oy=y
    oox=x
    ooy=y

    while True:
        plot_block(bg,oox,ooy)
        # plot_block(trail_2,ox,oy)
        # plot_block(trail_1,x,y)
        oox=ox
        ooy=oy
        ox=x
        oy=y
        x+=dx
        y+=dy
        plot_block(trail_2,oox,ooy)
        plot_block(trail_1,ox,oy)
        plot_block(ball_colour,x,y)
        
        while True:
            k=inkeys()
            if k=="":
                break
            if k == "z":
                dx=-1
                beep_key_left()
            if k == "x":
                dx=1
                beep_key_right()
            if k == "q":
                return
            if k == "k":
                bg=black
                for i in range(60):
                    for j in range(80): plot_block(bg,j,i)
            if k=="r":
                ball_colour, trail_1, trail_2, bg = red, midred, darkred, darkred
                for i in range(60):
                    for j in range(80): plot_block(bg,j,i)
            if k == "g":
                ball_colour, trail_1, trail_2, bg = green, midgreen, darkgreen, darkgreen
                for i in range(60):
                    for j in range(80): plot_block(bg,j,i)
            if k=="b":
                ball_colour, trail_1, trail_2, bg = blue, midblue, darkblue, darkblue
                for i in range(60):
                    for j in range(80): plot_block(bg,j,i)
    
        
        if x<=1:
            dx=1
            beep_wall()
        if x >= 39:
            dx=-1
            beep_wall()
        if y<=1:
            dy=1
            beep_top()
        if y >= 25:
            dy=-1
            beep_wall()
        sleep(sleeptime)
        
        print(chr(13)," "*x,"O", chr(13),end="")

bounce()
#