# Pico SMM

Simple MicroPython Microcomputer for the Raspberry Pi Pico

## Description

This is code to make the Raspberry Pi Pico act like a microcomputer similar to those from the 1980s.

It will require additional hardware to plug in a VGA monitor, sound output, and keyboard. (Independent keyboard support isn't included in the initial version, but a keyboard can be used through a serial terminal program on a USB-connected computer)

The system is be written to run on MicroPython, so it can be edited and modified without a separate C development environment.

One of the aims is to make the system easily understood - I've made the VGA output routine as simple as possible given that it uses the Pico's DMA and PIO systems to output the signal, and I'm planning to be selective about using any advanced features of MicroPython

## Connecting to a Pimoroni VGA demo board
The Pico isn't directly plugged into the VGA board as that doesn't allow a choice of which pins to wire where.

The following Pi Pico pins are connected to the corresponding demo board sockets:
* VSYS / VS
* GND / - (e.g. the GND nearest VSYS)
* 3V3 / +
* GP28 / 28 (audio)
* GP27 / 27 (audio)
* GP17 / 17 (VGA VSYNC)
* GP16 / 16 (VGA HSYNC)
* GP15 / 15 (VGA blue high bit)
* GP14 / 14 (VGA blue next bit)

(Pico label first, then demo board label)

These Pi Pico pins and demo board sockets are connected - they are different so that fewer pins are used for VGA than the demo board supports:

* GP13 / 10 (VGA green high bit)
* GP12 /  9 (VGA green next bit)
* GP11 /  4 (VGA red high bit)
* GP10 /  3 (VGA red next bit)

(Again, Pico label first, then demo board label)

## Improvements

I would like to support games or other programs not written in Python. This could involve a program-runner section, which either calls an arm assembly function, or emulates a 6502, or emulates a made-up CPU, or interprets a custom language (directly or after compiling to bytecode). Accessing audio and VGA could be through memory accesses from the emulated CPU, or function calls in the interpreted langauge.

SD card support would be good. Preferably simple support that can be integrated into the system and understood rather than used as a black box. The Pimoroni VGA demo board includes a microSD card slot.

PS/2 keyboard support would be good.

I'd like to improve the audio to be similar to the BBC Micro. Sampled sound isn't part of my vision for the microcomputer system, but may be good as an option or in a fork, for people who want to make a more advanced system. 

VGA improvements - the initial version has low horizontal resolution to save memory. This could be improved by repeating lines vertically to reduce vertical resolution, and/or by expanding a small part of the display memory at a time from a more compact representation, e.g. using a small palette, or excluding duplication of the sync and blanking signals. Most ways of improving the VGA output system will increase complexity, so I may want to also keep a simple version around.

Improve adaptability for different VGA connections - the current code assumes there are 8 bits of VGA output - 6 for colour, and bits 6 and 7 are HSYNC and VSYNC. Instead, it should adapt to the pin selection in the resources listed at the top (which may need to specify HSYNC and VSYNC pins). It should be made easy to adapt for the PicoMite VGA pins, for example, which uses 4 colour pins and 2 sync pins. Also for the standard wiring of the Pimoroni VGA demo board, which uses more colour pins.